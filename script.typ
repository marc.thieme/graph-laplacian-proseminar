#import "@local/theorems:0.0.1": init-theorems, zb, proof, rules, base, step, assume, project, break-page-after-chapters

#show: project.with([Proseminar Lineare Algebra in der Datenwissenschaft:\ Graph Laplacian], "Prof. Dr. Trabs", "Marc Thieme")
#show: break-page-after-chapters
#set math.equation(numbering: "(1.1)", supplement: "Gleichung")

#let If = "if"
#let Else = "else"
#let vol = "vol"
#let trans(x) = $#x^T$
#let one = $bb("1")$
#let limm(symbol, var) = $symbol_(var=1)^n$
#let summ = limm.with($sum$)

#let (definition, notation, slim, eigen, example, theorem, question) = init-theorems("theorems",
  definition: ("Definition", ),
  notation: ("Notation", ),
  question: ("Frage", ),
  eigen: ("Von mir",),
  example: ("Beispiel", ),
  theorem: ("Satz", ),
)

#counter(heading).step()

= Nachbarschaftsgraphen
== Grundlagen
#notation[
  / $x_i in V$: Ein Datenpunkt und gleichzeitig ein Vertex in Graph
  // / $s_(i j) >= 0$: notion of similarity
  // / Similarity Graph: Undirected Graph of datapoints with edges where $s_(i j) > *$
  / $A subset V$: Eine Zusammenhangskomponente des Graphen
  // / $overline(A subset V)$: The complement $V without A$: 
  / $one = vec(1, 1, dots.v, 1)$: Eins-Vektor
  / $f_A = vec(f_1\, ...\, f_n) in R^n$: The vector with entries $f_i = cases(f_i If x_i in A, 0 Else)$
  // / $i in A eq.triple { i | x_i in A}$: The set of indices of the datapoints
]

#definition[$W$][Gewichtsmatrix][
  Eine Matrix $W = (w_(i j))_(i, j in 1..n) >= 0$, sodass 
  $w_(i j) = cases("gewicht"(x_i, x_j) "wenn i und j verbunden sind", 0 "sonst")$.

  Da unsere Graphen Schleifenfrei sein werden, gilt: $w_(i i) = 0 quad (i in 1..n)$

  Bei einem ungewichteten Graphen sei $w_(i j) in {0, 1}$.
]

#definition[$D$][Degree Matrix][
  Die Diagonalmatrix $
mat(d_1,\ , 0; \ , dots.down, \ ; 0, \ , d_n)
$ wobei $deg(x_i)$ der Grad von $x_i$ im Graph ist.

Der Grad ist dabei (auch für gewichtete Graphen) als $
  d_i = summ(j) w_(i j)
$
definiert
]

// #definition[Size of Vertex sets][
//   + $|A| : "# of vertices in" A$
//   + $vol(A) &: sum_(i in A)d_i$
// ]

== Verschiedene Ähnlichkeitsgraph-Konstruktionen
Ein Ähnlichkeitsgraph ist ein Graph, dessen Kanten und Gewichte eine Ähnlichkeitsbeziehung beschreiben.

#definition[$epsilon$-Nachbarschaftsgraph][
  Verbinde alle Knoten mit Distanz weniger als $epsilon$.\
  Dieser Graph macht nur ungewichtet Sinn, da die Gewichte ohnehin alle $epsilon$-nah beieinander lägen.
]

#definition[$k$-nearest neighbor graph][
Jeder Knoten ist mit seinen $k$ nächsten Nachbarn verbunden. Da diese Relation nicht symmetrisch ist, 
muss man hier unterscheiden zwischen:
  / $k$-nearest neighbour graph: Jede gerichtete Kante wird in eine ungerichtete verwandelt
  / mutual $k$-nearest neighbour graph: Nur bidirektionale Kanten werden als ungerichtete Kanten behalten
]

#definition[Fully connected Graph][
  Jeder Knoten ist mit jedem anderen Knoten verbunden.
  // Only useful when the similarity function models local neighborhoods.

  Das macht nur Sinn, wenn die Distanzbezihung lokale Nachbarschaften modelliert (Keine Ahnung was das bedeuten soll).

  Z.b die Gaussian similarity function $s(x_i, x_j) = exp(-lr(||x_i - x_j ||)^2 / (2 sigma^2))$ where $sigma$ controls the width of the neighborhood
]

#question[][
  Was bedeutet "Lokale Nachbarschaft"?
  Ich finde das weder im Internet. Für mich ist auch nicht intuitiv, warum nicht jede beliebige Ähnlichkeitsfunktion\
  sinnvoll wäre.
]

= Graph Laplacian
Wir setzen in diesem Kapitel einen
*Ungerichteten Graph* $G$ mit Gewichtsmatrix $W >= 0$ voraus.

#definition[Unnormalized Graph Laplacian][
  $L = D - W$
]

#example[][
$
D := mat(3, 0, 0, 0; 0, 2, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
quad
W := mat(0, 1/2, 1, 2; 1/2, 0, 1, 0; 1, 1, 0, 0; 2, 0, 0, 0)
\
L = D - W = mat(3, -1/2, -1, -2; -1/2, 2, -1, 0; -1, -1, 2, 0; -2, 0, 0, 1)
$
]
#theorem[Graph Laplacian][Eigenschaften][
  + Mit $f in RR^n$: $
    trans(f) L f = 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i - f_j)^2
  $
  + L ist symmetrisch und positiv semi-definit
  + Der kleinste Eigenwert von L ist 0 mit Eigenvektor #one
  + Eigenwerte von $L$ sind nicht-negativ und reellwertig
]
#proof[

1.\
  $
    trans(f) L f &= trans(f) D f - trans(f) W f 
    = 1/2 (2 trans(f) D f - 2 trans(f) W f )
    \ &= 1/2 (trans(f) D f - 2 trans(f) W f + trans(f) D f)
    = 1/2 (sum_(i=1)^n d_i f_i^2 - 2 sum_(i = 1)^n sum_(j = 1)^n w_(i j) f_i f_j + sum_(j=1)^n d_j f_j^2)
    \ &= 1/2 (sum_(i=1)^n sum_(j=1)^n w_(i j) f_i^2 - 2 sum_(i = 1)^n sum_(j = 1)^n w_(i j) f_i f_j + sum_(j=1)^n sum_(i=1)^n w_(j i) f_j^2)
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i ^2 - 2 f_i f_j + f_j ^2)
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i - f_j)^2
  $ <quadratic-identity>

2. $L = D - W$ ist symmetrisch folgt aus $D$ und $W$ symmetrisch.
  $L$ ist positiv semi-definit gdw. $trans(f) L f >= 0 quad forall_(f in R^n)$.
  Dies ist die gleiche Form, wie in 1. Da $w_(i j) >= 0 forall_(i j)$, ist die Bedingung erfüllt.

3. Wir berechnen $
  L one = D one - W one = vec(d_1, dots.v, d_n) - vec(summ(i) w_(1 i), dots.v, summ(i) w_(n i)) 
  = vec(d_1, dots.v, d_n) - vec(d_1, dots.v, d_n) = 0
$
  Somit ist $one = vec(1, dots.v, 1)$ ein Eigenvektor zum Eigenwert 0.
4. Trivial mit 1 bis 3
]

#theorem[\# Connected Components][
  Seien $A_1, ..., A_k$ die Zusammenhangskomponenten von $G$.\
  Der Eigenraum des Eigenwerts 0 ist $[one_A_1, ..., one_A_k]$.\
  Insbesondere ist die Multiplizität des Eigenraums zu 0 die Anzahl der Zusammenhangskomponenten $k$.\
]
#proof[
  Wir zeigen zuerst, dass jeder Eigenwert zu 0 eine Linearkombination aus ${one_A_1, ..., one_A_k}$ ist.
  Sei $f$ ein Eigenvektor von $L$ zum Eigenwert $0$. Dann ist $
    1/2 summ(i) summ(j) w_(i j) (f_i - f_j)^2 = trans(f) overbrace(L f, 0) = 0
  $
  Jeder Term ist $>= 0$, also genau gleich 0.
  Also muss für alle $i, j$, für die $w_(i j) != 0$, gelten $f_i = f_j$ $
    limm(forall, i) limm(forall, j) quad w_(i j) = 0 or f_i = f_j
  $ <eigenvector-bound>
  Sei nun $x_i in V$ und $A_m$ seine Zusammenhangskomponente.
  Wir können induktiv über die kürzestes Pfadlänge $n$ von Pfaden ab $x_i$ zeigen, 
  dass für alle $x_j in A_m$ gilt $f_i = f_j$.

  \ #base $n=0$ Der kürzeste Pfad der Länge 0 endet in $x_i$. Offensichtlich ist $f_i = f_i$
  \ #assume Für jeden kürzesten Pfad der Länge $n$ von $x_i$ zu einem Knoten $x_j$, ist $f_i = f_j$
  \ #step[n] Sei $P = underbracket(x_i\, ...\, x_h, P')\, x_j$ ein kürzester Pfad der Länge $n + 1$ zu einem Knoten $x_j$.
  Derweil ist $P'$ ein kürzester Pfad von $x_i$ zu $x_h$, sonst könnte man einfach einen kürzeren Pfad
  als $P$ zu $x_h$ konstruieren.

  Per Induktionsvorr. ist daher $f_i = f_h$.
  Da eine Kante von $x_h$ zu $x_j$ ex. ist laut @eigenvector-bound daher $f_h = f_j$.
  Somit ist $f_i = f_h = f_j$.

  Zu allen Knoten, zu denen es einen Pfad ab $x_i$ gibt, ex. auch ein kürzester Pfad
  ab $x_i$. 
  Damit haben wir gezeigt, dass für alle Knoten $x_j$ in $A_m$ gilt, dass $f_i = f_j$.

  Seien $(I_i)^k_(i=1)$ Indices, sodass $x_I_i in A_i$. 
  Mit letztem Resultat können wir also jeden Eigenvektor $f$ schreiben, als $
    f = f_I_1 one_A_1 + f_I_2 one_A_2 + ... + f_I_k one_A_k = vec(f_I_1, dots.v, f_I_1, f_I_2, dots.v, f_I_2, dots.v)
  $

  Nun zeigen wir, dass jede Linearkombination aus ${one_A_1, ..., one_A_k}$ auch ein Eigenvektor ist.

  Sei $f = a_1 one_A_1 + ... + a_k one_A_k = summ(i) a_i one_A_i$. Wir rechnen nach $
    L f &= a_1 L one_A_1 + ... + a_k L one_A_k = summ(k) a_i L one_A_i = summ(i) a_i (D one_A_i -W one_A_i)
    \ &= summ(i) a_i (sum_(j in A_i) d_j - sum_(j in A_i) summ(h)w_(j h)) = summ(i) a_i (sum_(j in A_i) summ(h) w_(j h) - sum_(j in A_i) summ(h)w_(j h))
    \ &= summ(i) a_i dot 0 = 0
  $

  Somit haben wir gezeigt, dass $[one_A_1, ..., one_A_k]$ den Eigenraum von $L$ zum Eigenwert 0 aufspannt und damit
  ${one_A_1, ..., one_A_k}$ eine Basis dessen ist.
  Da jeder Knoten zu genau einer Zusammenhangskomponente gehört, sind diese Vektoren auch linear
  Unabhängig und der Eigenraum hat Dimension $k$.
]
\
Alternativ:
#proof[
  Gibt es mehrere Zusammenhangskomponenten, hat $L$ bei richtiger Spaltenwahl die Blockgestalt $mat(L_1, , 0;\ , dots.down, ; 0, , L_k)$, wobei
  $L_i$ die Laplacian Matrix von einer Zusammenhangskomponente ist. 

  Denn zwischen Zusammenhangskomponenten gibt es keine Kanten. 
  Daher ist $W$ in allen diesen Einträgen 0 und dadurch auch die Laplacian.

  Durch nachrechnen haben wir bereits gesehen, dass jede Laplacian 0 als Eigenvektor zum Eigenwert 0 hat.
  Daraus folgt, für jede Blocklaplacian $L_i$ eine Nullzeile in der diagonalisierten Matrix entsteht mit
  dem Eigenvektor $one_A_k$.
]

== Normalisierte Graph Laplacian
#let Lsym = $L_"sym"$
#let Lrw = $L_"rw"$
#definition[2 Varianten][Normalisierte Graph Laplacians][
  $
    Lsym &:= D^(-1/2) L D^(-1/2) = I - D^(-1/2) W D^(-1/2)\
    Lrw &:= D^(-1) L = I - D^(-1) W
  $
]
Dabei ist #Lsym eine symmetrische Matrix.

#question[][Aber $D^(-1)$ ist doch nur definiert, wenn jeder Knoten mindestens Grad 1 hat.
Sonst würde man doch durch 0 teilen, oder?
Hierauf wird in dem Paper gar nicht eingegangen. Auf Wikipedia wird hier die Moore-Penrose-Inverse auch verwendet.]

#example[][
$
D := mat(3, 0, 0, 0; 0, 2, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
quad
W := mat(0, 1/2, 1, 2; 1/2, 0, 1, 0; 1, 1, 0, 0; 2, 0, 0, 0)
quad
L = D - W = mat(3, -1/2, -1, -2; -1/2, 2, -1, 0; -1, -1, 2, 0; -2, 0, 0, 1)
\
Lsym = D^(-1/2) L D^(-1/2) = mat(1, -√6/12, -√6/6, -2√3/3; -√6/12, 1, -1/2, 0; -√6/6, -1/2, 1, 0; -2√3/3, 0, 0, 1)
\
Lrw = D^(-1) L = mat(1, -1/6, -1/3, -2/3; -1/4, 1, -1/2, 0; -1/2, -1/2, 1, 0; -2, 0, 0, 1)
$
]
#theorem[Eigenschaften der normalisierten Graph Laplacians][
  1. Let $f in RR^n$: $
    trans(f) Lsym f = 1/2 summ(i) summ(j) w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2
  $
  2. $lambda$ ist ein Eigenwert von #Lrw mit Eigenvektor $u$ gdw. $lambda$ ein Eigenwert von #Lsym
    mit Eigenvektor $w = D^(1/2) u$ ist.
  3. $lambda$ ist ein Eigenwert von #Lrw mit Eigenwert $u$ gdw. $lambda$ und $u$ das verallgemeinerte
    Eigenwertproblem $L u = lambda D u$ lösen.
  4. 0 ist ein Eigenwert von #Lrw mit $one$ als Eigenvector. 
    0 ist ein Eigenwert von #Lsym mit Eigenvektor $D^(1/2) one$
  5. #Lsym und #Lrw sind positiv semi-definit und haben $n$ nicht-negative reellwertige Eigenwerte.
]
#proof[
1. 
$
    trans(f) Lsym f &= trans(f) D^(-1/2) L D^(-1/2) f
    = trans((D^(-1/2) f)) L (D^(-1/2)f) \
#[Mit Identiät @quadratic-identity aus vorigem Abschnitt]\
    &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) ((D^(-1/2) f)_i - (D^(-1/2) f)_j)^2\
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (d_i^(-1/2) f_i - d_j ^ (-1/2) f_j)^2\
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2\
  $  
#let DD = $D^(-1/2)$
2. Sei $f, u in RR^n$ sodass $f = D^(1/2) u$. Wir rechnen $
  Lsym D^(1/2)u &= DD L DD D^(1/2) u &= DD L u \
  Lrw u &= D^(-1) L u &= D^(-1/2) D^(-1/2) L u
$
Äquivalenzumformungen liefern: $
  &Lsym D^(1/2) u &= lambda D^(1/2) u &\
  &<=> DD L u &= lambda D^(1/2) u quad &| D^(-1/2) dot \
  &<=> D^(-1) L u &= lambda u &\
  &<=> Lrw u &= lambda u&
$
3. Sei $u in RR^n, lambda in RR$. Äquivalenzumformungen $
  &<=> Lrw u &= lambda u & \
  &<=> D^(-1) L u &= lambda u quad &| D dot \
  &<=> L u &= D lambda u & \
  &&= lambda D u & \
$
4. Wir rechnen nach: $
  Lrw one &= D^(-1) L one = D^(-1) 0 &q,= 0 \
  Lsym D^(1/2) one &= DD L DD D^(1/2) one 
  = DD L one = DD 0 &q,= 0
$
5. $Lsym$ ist positiv semi-definit mit $n$ nicht-negativen reellwertigen Eigenwerten nach 1. und mit 2. gilt dass dann auch für $Lrw$ 
]
// D^(-1/2) L D^(-1/2) 
#theorem[\# Zusammenhangskomponenten von $Lsym$ and #Lrw][
  Sei $G$ ein ungerichteter Graph mit Zusammenhangskomponenten $A_1, ..., A_k$.

  Der Eigenraum von #Lrw wird von den Vektoren $one_A_1, ..., one_A_k$
  aufgespannt. Der Eigenraum von #Lsym von $quad D^(1/2) one_A_i quad (i in 1..k)$
]
Zuerst #Lsym:
#proof[
  Wir zeigen zuerst, dass jeder Eigenwert zu 0 eine Linearkombination aus ${one_A_1, ..., one_A_k}$ ist.\
  Sei $f$ ein Eigenvektor von $Lsym$ zum Eigenwert $0$. Dann ist $
    1/2 summ(i) summ(j) w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2 = trans(f) overbrace(Lsym f, 0) = 0
  $
  Jeder Term ist $>= 0$, also genau gleich 0.
  Also muss für alle $i, j$, für die $w_(i j) != 0$, gelten $f_i/sqrt(d_i) = f_j/sqrt(d_j)$ $
    limm(forall, i) limm(forall, j) quad w_(i j) = 0 or f_i/sqrt(d_i) = f_j/sqrt(d_j)
  $
  Sei nun $x_i in V$ und $A_m$ seine Zusammenhangskomponente.

  Wie zuvor können wir induktiv zeigen, dass für jeden Knoten $x_j$ in $A_m$ gilt$
   f_j = sqrt(d_j)f_i/sqrt(d_i) $
  Die Induktion schreiben wir hier nicht erneut auf.

  Somit können wir $f_A_m$ schreiben, als $
    f_A_m = f_i / sqrt(d_i) D^(1/2) one_A_m
  $

  Seien $(I_i)^k_(i=1)$ Indices, sodass $x_I_i in A_i$. 
  Mit letztem Resultat können wir also jeden Eigenvektor $f$ schreiben, als $
    f = f_I_1 / sqrt(d_i) one_A_1 + f_I_2/sqrt(d_2) D_A_2 one_A_2 + ... + f_I_k/sqrt(d_k) D_A_k one_A_k
  $

  Nun zeigen wir, dass jede Linearkombination aus ${D^(1/2)one_A_1, ..., D^(1/2)one_A_k}$ auch ein Eigenvektor ist.

  Sei $f = a_1 D^(1/2) one_A_1 + ... + a_k D^(1/2) one_A_k = summ(i) a_i D^(1/2) one_A_i$. Wir rechnen nach $
    Lsym f &= a_1 D^(-1/2)L D^(-1/2) one_A_1 + ... + a_k D^(-1/2) L D^(-1/2) one_A_k = summ(k) a_i D^(-1/2) L D^(-1/2) D^(1/2) one_A_i 
    \ &= summ(k) a_i D^(-1/2) L one_A_i 
    = summ(i) a_i D^(-1/2) (D one_A_i - W one_A_i)
    \ &= summ(i) a_i D^(-1/2) (sum_(j in A_i) d_j - sum_(j in A_i) summ(h)w_(j h)) 
    = summ(i) a_i D^(-1/2) (sum_(j in A_i) summ(h) w_(j h) - sum_(j in A_i) summ(h)w_(j h))
    \ &= summ(i) a_i D^(-1/2) dot 0 = 0
  $

  Somit haben wir gezeigt, dass $[D^(1/2)one_A_1, ..., D^(1/2)one_A_k]$ den Eigenraum von $L$ zum Eigenwert 0 aufspannt und damit
  ${D^(1/2)one_A_1, ..., D^(1/2)one_A_k}$ eine Basis dessen ist.
  Da jeder Knoten zu genau einer Zusammenhangskomponente gehört, sind diese Vektoren auch linear
  Unabhängig und der Eigenraum hat Dimension $k$.
]
Und nun #Lrw
#proof[
  Sei $f in RR^n$
  Im letzten Theorem haben wir gesehen, dass dann gilt $
Lrw f = 0 quad <=> quad L f = 0 dot D dot f = 0
$
Somit ist der Eigenraum von #Lrw gleich dem Eigenraum von $L$.
]

