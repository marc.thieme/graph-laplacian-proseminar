#import "@preview/polylux:0.3.1": *
#import "@local/theorems:0.0.1": *
#import themes.simple: *

#let Lsym = $L_"sym"$
#let Lrw = $L_"rw"$
#show: rules
#show "EW": [_EW_]
#show "EV": [_EV_]
#set page(paper: "presentation-16-9")
#set text(size: 20pt)
// #set math.equation(numbering: "(1.1)", supplement: "Gleichung")
// #set heading(numbering: "1.1")
// #show "=": $#h(5mm) = #h(5mm)$

#let zb = zb + h(0.8cm)
#let obo(..args) = one-by-one(..args)
#let If = "if"
#let Else = "else"
#let vol = "vol"
#let trans(x) = $#x^T$
#let one = $bb("1")$
#let limm(symbol, var) = $symbol_(var=1)^n$
#let summ = limm.with($sum$)

#let (definition, recall, notation, slim, eigen, example, theorem, question) = init-theorems("theorems",
  definition: ("Definition", ),
  notation: ("Notation", ),
  question: ("Frage", ),
  eigen: ("Von mir",),
  example: ("Beispiel", ),
  theorem: ("Satz", ),
  claim: ("Behauptung",),
  recall: ("Erinnerung", )
)
#let (instruction, slim: sinstr) = init-theorems("Anweisungen",
  instruction: ("Anweisung", yellow)
)

#let instr(..args, title, body) = centered-slide(instruction(..args, [Nicht in Live Präsentation], title, body))
#let ininstr(..args, title, body) = instruction(..args, [Nicht in Live Präsentation], title, body)
#let sliminstr(..args, title) = (sinstr.instruction)(..args, [Nicht in Live Präsentation], title)

#counter(heading).step()

#polylux-slide[
#set align(center + horizon)
= Graph Laplacian
== Grundlagen
  
]
#polylux-slide[
#notation[Grundlagen][
  / $x_i in V$: Ein Datenpunkt und gleichzeitig ein Vertex in Graph
  // / $s_(i j) >= 0$: notion of similarity
  // / Similarity Graph: Undirected Graph of datapoints with edges where $s_(i j) > *$
  / $A subset V$: Eine Zusammenhangskomponente des Graphen
  // / $overline(A subset V)$: The complement $V without A$: 
  / $one = vec(1, 1, dots.v, 1)$: Eins-Vektor
  / $f_A = vec(f_1\, ...\, f_n) in R^n$: The vector with entries $f_i = cases(f_i If x_i in A, 0 Else)$

  / $one_A = vec(one_1\, ...\, one_n) in R^n$: The vector with entries $one_i = cases(one_i If 1 in A, 0 Else)$
  // / $i in A eq.triple { i | x_i in A}$: The set of indices of the datapoints
]]

#sliminstr[Tafel][Abschreiben]

#centered-slide[
  == Gewichts- und Degree Matrix
  \
  #sliminstr[Tafel][Beispiel mit Graph anmalen]
]

#centered-slide[
#definition[$W$][Gewichtsmatrix][
  Eine Matrix $W$ mit nicht-negativen Einträgen, sodass 
  $ w_(i j) = cases("Gewicht"(x_i, x_j) "wenn i und j verbunden sind", 0 "sonst") $

  Da schleifenfrei: $w_(i i) = 0 quad (i in 1..n)$

  Bei einem ungewichteten Graphen sei $w_(i j) in {0, 1}$.
]]

#centered-slide[
#definition[$D$][Degree Matrix][
  Die Diagonalmatrix $
mat(d_1,\ , 0; \ , dots.down, \ ; 0, \ , d_n),
$ wobei $d_(x_i)$ der Grad von $x_i$ im Graph ist, heißt Degree-Matrix

Der Grad ist dabei als $
  d_i = summ(j) w_(i j)
$
definiert
]
]

#centered-slide[
  = Ähnlichkeitsgraphen
]

#centered-slide[
#definition[$epsilon$-Nachbarschaftsgraph][
  Verbinde alle Knoten mit Distanz kleiner als ein $epsilon$.\
  Dieser Graph macht nur ungewichtet Sinn, da die Gewichte ohnehin alle $epsilon$-nah beieinander lägen.
]

#definition[$k$-nearest neighbor graph][
  / $k$-nearest neighbour graph: Jede gerichtete Kante wird in eine ungerichtete verwandelt
  / mutual $k$-nearest neighbour graph: Nur bidirektionale Kanten werden als ungerichtete Kanten behalten
]
]

#centered-slide[
#definition[Fully connected Graph][
  Jeder Knoten ist mit jedem anderen Knoten verbunden.

  Distanzmaß/Gewichte modellieren _lokale Nachbarschaften_

  #zb $s(x_i, x_j) = exp(-lr(||x_i - x_j ||)^2 / (2 sigma^2))$, für Nachbarschaftsweite $sigma$
]
]

#centered-slide[
  == Graph Laplacian
  #v(2cm)
#pause
#definition[Unnormalized Graph Laplacian][
  $L = D - W$
]
]
#centered-slide[
$ obo(
W := mat(0, 2, 1, 1; 2, 0, 1, 0; 1, 1, 0, 0; 1, 0, 0, 0)
quad
D = mat(4, 0, 0, 0; 0, 3, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
\ ,
#v(8cm)

L = D - W = mat(4,  -2, -1, -1; -2, 3,  -1, 0; -1, -1, 2,  0; -1, 0,  0,  1)
)

 // #example[][$ obo(
// D := mat(3, 0, 0, 0; 0, 2, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1),
// quad
// W := mat(0, 1/2, 1, 2; 1/2, 0, 1, 0; 1, 1, 0, 0; 2, 0, 0, 0),
// \
// L = D - W = mat(3, -1/2, -1, -2; -1/2, 2, -1, 0; -1, -1, 2, 0; -2, 0, 0, 1)
$ ]

#instr[mündlich][Hinweisen][
  1. Es ist symmetrisch
  2. Jede Zeile/ Spalte summiert auf 0
]

#centered-slide[
#theorem[Graph Laplacian][4 Eigenschaften][#obo[
  1. Mit $f in RR^n$: $
    trans(f) L f = 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i - f_j)^2
  $][

#v(8mm)
  
  2. L ist symmetrisch und positiv semi-definit][

#v(8mm)
  
  3. Der kleinste EW von L ist 0 mit EV #one][
#v(8mm)
  
  4. EW von $L$ sind nicht-negativ und reellwertig
]]
]

#instr[Tafel][Abschreiben][
  Schreibe alle Eigenschaften ab, um in den späteren Beweis darauf zurückkommen zu können
]
// first identity
#polylux-slide[$obo(
    trans(f) L f, &= trans(f) D f - trans(f) W f ,
    = 1/2 (2 trans(f) D f - 2 trans(f) W f ),
    = 1/2 (trans(f) D f - 2 trans(f) W f + trans(f) D f),
    \ &= 1/2 (sum_(i=1)^n d_i f_i^2 - 2 sum_(i = 1)^n sum_(j = 1)^n w_(i j) f_i f_j + sum_(j=1)^n d_j f_j^2),
    \ &= 1/2 (sum_(i=1)^n sum_(j=1)^n w_(i j) f_i^2 - 2 sum_(i = 1)^n sum_(j = 1)^n w_(i j) f_i f_j + sum_(j=1)^n sum_(i=1)^n w_(j i) f_j^2),
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i ^2 - 2 f_i f_j + f_j ^2),
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i - f_j)^2,
)$]
#centered-slide[
#set align(left)
2. $L$ symmetrisch trivial. $L$ positiv semi-definit gdw. $quad trans(f) L f >= 0 quad forall_(f in R^n)$

#pause
#v(2cm)
3. $
  L one = D one - W one = vec(d_1, dots.v, d_n) - vec(summ(i) w_(1 i), dots.v, summ(i) w_(n i)) 
  = vec(d_1, dots.v, d_n) - vec(d_1, dots.v, d_n) = 0
$

#pause
  Somit ist $one = vec(1, dots.v, 1)$ ein EV zum EW 0.
#v(2cm)
#pause
4. Trivial mit 1 bis 3
]
#centered-slide[
#theorem[\# Connected Components][
  Seien $A_1, ..., A_k$ die Zusammenhangskomponenten von $G$.\
  Der Eigenraum des EWs 0 ist $[one_A_1, ..., one_A_k]$.\
  Insbesondere ist die Multiplizität des Eigenraums zu 0 die Anzahl der Zusammenhangskomponenten $k$.\
]
#sliminstr[Tafel][Abschreiben]
]
#polylux-slide[
// #sliminstr[Tafel][EW zu 0 ist LK aus ${one_A_1, ..., one_A_k}$ ist.]
#v(-6mm)
#(slim.claim)[Jeder EW zu 0 ist eine LK aus ${one_A_1, ..., one_A_k}$]
#pause
#v(-3mm)
  Sei $f$ ein EV von $L$ zum EW $0$. 
  #pause Dann ist $
    1/2 summ(i) summ(j) w_(i j) (f_i - f_j)^2 = trans(f) overbrace(L f, 0) = 0
  $
  // Jeder Term ist $>= 0$, also genau gleich 0.
  #pause
  // Also muss für alle $i, j$, für die $w_(i j) != 0$, gelten $f_i = f_j$ 
  $
    ==> quad limm(forall, i) limm(forall, j) quad w_(i j) != 0 => f_i = f_j
  $ <eigenvector-bound>
  #pause
 \
  Sei $x_i in V$ und $A_m$ seine Zusammenhangskomponente.
  #pause
  // Wir können induktiv über die kürzestes Pfadlänge $n$ von Pfaden ab $x_i$ zeigen, 
  // dass für alle $x_j in A_m$ gilt $f_i = f_j$.

  \
  Induktion über Pfadlänge zeigt:
  #pause
  $
    forall_(x_j in A_m) : f_i = f_j
  $
]
#instr[Tafel][Beweis][
#set text(18pt)
  #base $n=0$ Der kürzeste Pfad der Länge 0 endet in $x_i$. Offensichtlich ist $f_i = f_i$
  \ #assume Für jeden kürzesten Pfad der Länge $n$ von $x_i$ zu einem Knoten $x_j$, ist $f_i = f_j$
  \ #step[n] Sei $P = underbracket(x_i\, ...\, x_h, P')\, x_j$ ein kürzester Pfad der Länge $n + 1$ zu einem Knoten $x_j$.
  Derweil ist $P'$ ein kürzester Pfad von $x_i$ zu $x_h$, sonst könnte man einfach einen kürzeren Pfad
  als $P$ zu $x_h$ konstruieren.

  Per Induktionsvorr. ist daher $f_i = f_h$.
  Da eine Kante von $x_h$ zu $x_j$ ex. ist vorigem Ergebnis daher $f_h = f_j$.
  Somit ist $f_i = f_h = f_j$.

  Zu allen Knoten, zu denen es einen Pfad ab $x_i$ gibt, ex. auch ein kürzester Pfad
  ab $x_i$. 
  Damit haben wir gezeigt, dass für alle Knoten $x_j$ in $A_m$ gilt, dass $f_i = f_j$.
]
#polylux-slide[
  Seien $(I_i)^k_(i=1)$ Indices, sodass $x_I_i in A_i$. Dann
  $
    f = f_I_1 one_A_1 + f_I_2 one_A_2 + ... + f_I_k one_A_k = vec(f_I_1, dots.v, f_I_1, f_I_2, dots.v, f_I_2, dots.v)
  $

  #align(right, square(width: 0.5cm))

]
#polylux-slide[
  #(slim.claim)[Jede LK aus ${one_A_1, ..., one_A_k}$ ist auch ein EV]
  #pause
\
  Sei $f = a_1 one_A_1 + ... + a_k one_A_k$ 
  #pause

  Nachrechnen:$ obo(start: #3,
    L f , &= a_1 L one_A_1 + ... + a_k L one_A_k , = summ(k) a_i L one_A_i , = summ(i) a_i (D one_A_i -W one_A_i)
    \ , &= summ(i) a_i (sum_(j in A_i) d_j - sum_(j in A_i) summ(h)w_(j h)) , = summ(i) a_i (sum_(j in A_i) summ(h) w_(j h) - sum_(j in A_i) summ(h)w_(j h))
    \ , &= summ(i) a_i dot 0 , = 0
  ) $
]
#instr[Mündlich][Beweisende][
  Somit haben wir gezeigt, dass $[one_A_1, ..., one_A_k]$ den Eigenraum von $L$ zum EW 0 aufspannt und damit
  ${one_A_1, ..., one_A_k}$ eine Basis dessen ist.
  Da jeder Knoten zu genau einer Zusammenhangskomponente gehört, sind diese Vektoren auch linear
  Unabhängig und der Eigenraum hat Dimension $k$.
]

#instr[Mündlich/Tafel][Alternative][
#proof[
  Gibt es mehrere Zusammenhangskomponenten, hat $L$ bei richtiger Spaltenwahl die Blockgestalt $mat(L_1, , 0;\ , dots.down, ; 0, , L_k)$, wobei
  $L_i$ die Laplacian Matrix von einer Zusammenhangskomponente ist. 

  Denn zwischen Zusammenhangskomponenten gibt es keine Kanten. 
  Daher ist $W$ in allen diesen Einträgen 0 und dadurch auch die Laplacian.

  Durch nachrechnen haben wir bereits gesehen, dass jede Laplacian 0 als EV zum EW 0 hat.
  Daraus folgt, für jede Blocklaplacian $L_i$ eine Nullzeile in der diagonalisierten Matrix entsteht mit
  dem EV $one_A_k$.
]
]
#polylux-slide[
== Normalisierte Graph Laplacians

#pause
#set align(horizon)
#definition[2 Varianten][Normalisierte Graph Laplacians][
  $
    Lsym &:= D^(-1/2) L D^(-1/2) = I - D^(-1/2) W D^(-1/2)\
    Lrw &:= D^(-1) L = I - D^(-1) W
  $
]
]

#centered-slide[
  
$ obo(
W := mat(0, 2, 1, 1; 2, 0, 1, 0; 1, 1, 0, 0; 1, 0, 0, )
quad
D = mat(4, 0, 0, 0; 0, 3, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
quad
L = mat(4,  -2, -1, -1; -2, 3,  -1, 0; -1, -1, 2,  0; -1, 0,  0,  1)
\ ,
Lsym =mat(1, -sqrt(3)/3, -sqrt(2)/4, -1/2; -sqrt(3)/3, 1, -sqrt(6)/6, 0; -sqrt(2)/4, -sqrt(6)/6, 1, 0; -1/2, 0, 0, 1)
quad 
Lrw = mat(1, -1/2, -1/4, -1/4; -2/3, 1, -1/3, 0; -1/2, -1/2, 1, 0; -1, 0, 0, 1)
) 
$
]

#polylux-slide(
theorem[Eigenschaften der normalisierten Graph Laplacians][
  1. Let $f in RR^n$: $
    trans(f) Lsym f = 1/2 summ(i) summ(j) w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2
  $
  #pause
  2. $lambda$ ist EW von #Lrw mit EV $quad u quad <=> quad lambda$ ist EW von #Lsym
    mit EV $w = D^(1/2) u$  #pause

  3. $lambda$ ist ein EW von #Lrw mit EW $u$ gdw. $lambda$ und $u$ das verallgemeinerte
    Eigenwertproblem $L u = lambda D u$ lösen.

  #pause

  4. 0 ist ein EW von #Lrw mit $one$ als Eigenvector. 
    0 ist ein EW von #Lsym mit EV $D^(1/2) one$

  #pause

  5. #Lsym und #Lrw sind positiv semi-definit und haben $n$ nicht-negative reellwertige EWe.
]
)

#instr[Tafel][Abschreiben][
  Braucht ein bisschen aber man muss es halt noch sehen, wenn die Beweise erscheinen
]
#let DD = $D^(-1/2)$

#polylux-slide[
1. 
$ obo(
    trans(f) Lsym f , &= trans(f) D^(-1/2) L D^(-1/2) f,
    = trans((D^(-1/2) f)) L (D^(-1/2)f) , \
    &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) ((D^(-1/2) f)_i - (D^(-1/2) f)_j)^2, \
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (d_i^(-1/2) f_i - d_j ^ (-1/2) f_j)^2, \
    \ &= 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2\
  ) $  
]
#polylux-slide[
2. Sei $f, u in RR^n$ mit $f = D^(1/2) u$

#pause
\
Gegeben:
$
  Lsym D^(1/2)u &= DD L DD D^(1/2) u &= DD L u \
  Lrw u &= D^(-1) L u &= D^(-1/2) D^(-1/2) L u
$
#pause #line(length: 100%, stroke: (dash: "dashed", paint: gray, thickness: 0.2pt))
$ obo(start:#3,
  &Lsym D^(1/2) u &= lambda D^(1/2) u &\ ,
  &<=> DD L u &= lambda D^(1/2) u quad &| D^(-1/2) dot \ ,
  &<=> D^(-1) L u &= lambda u &\ ,
  &<=> Lrw u &= lambda u&,
  )
$]
#polylux-slide[
3. Sei $u in RR^n, lambda in RR$. Äquivalenzumformungen $ obo(
  & Lrw u &= lambda u & , \
  &<=> D^(-1) L u &= lambda u quad &| D dot , \
  &<=> L u &= D lambda u & , \
  &&= lambda D u & \
  )
$
#uncover("5-")[
4. Wir rechnen nach: $ obo(start: #6,
  Lrw one &= D^(-1) L one = D^(-1) 0 q= 0 ,\
  Lsym D^(1/2) one &= DD L DD D^(1/2) one 
  = DD L one = DD 0 q,= 0)
$
]
#uncover("8-")[
5. $Lsym$ positiv semi-definit: 1\
  $Lrw$ auch wegen 2
]
]

// zusammenhangskomponenten
#polylux-slide[
#theorem[\# Zusammenhangskomponenten von $Lsym$ and #Lrw][
  Sei $G$ ein ungerichteter Graph mit Zusammenhangskomponenten $A_1, ..., A_k$.

  Der Eigenraum von #Lrw wird von den Vektoren $one_A_1, ..., one_A_k$
  aufgespannt. 
  
  Der Eigenraum von #Lsym von $quad D^(1/2) one_A_i quad (i in 1..k)$
]
#pause

#v(1fr)

*Zuerst nur der Beweis für $Lsym$*:
#v(1fr)
]
#polylux-slide[
  Sei $f$ ein EV von $Lsym$ zum EW $0$. Dann ist $
    1/2 summ(i) summ(j) w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2 = trans(f) overbrace(Lsym f, 0) = 0
  $

  #pause

  $
    limm(forall, i) limm(forall, j) quad w_(i j) = 0 or f_i/sqrt(d_i) = f_j/sqrt(d_j)
  $

  #pause
  Sei nun $x_i in V$ und $A_m$ seine Zusammenhangskomponente.

  Wie zuvor: Für jeden Knoten $x_j$ in $A_m$ gilt$
   f_j = sqrt(d_j)f_i/sqrt(d_i) $
]
#polylux-slide[

  Somit können wir $f_A_m$ schreiben, als $
    f_A_m = f_i / sqrt(d_i) D^(1/2) one_A_m
  $

  Seien $(I_i)^k_(i=1)$ Indices, sodass $x_I_i in A_i$. 
$
    f = f_I_1 / sqrt(d_i) one_A_1 + f_I_2/sqrt(d_2) D_A_2 one_A_2 + ... + f_I_k/sqrt(d_k) D_A_k one_A_k
  $
]
#polylux-slide[
  #(slim.claim)[${D^(1/2)one_A_1, ..., D^(1/2)one_A_k}$ ist EW von 0]
  #pause

  Sei $f = a_1 D^(1/2) one_A_1 + ... + a_k D^(1/2) one_A_k = summ(i) a_i D^(1/2) one_A_i$
    #pause
  $ obo(start: #3, 
    Lsym f , &= a_1 D^(-1/2)L D^(-1/2) one_A_1 + ... + a_k D^(-1/2) L D^(-1/2) one_A_k , = summ(k) a_i D^(-1/2) L D^(-1/2) D^(1/2) one_A_i 
    \ , &= summ(k) a_i D^(-1/2) L one_A_i 
    , = summ(i) a_i D^(-1/2) (D one_A_i - W one_A_i)
    \ , &= summ(i) a_i D^(-1/2) (sum_(j in A_i) d_j - sum_(j in A_i) summ(h)w_(j h)) 
    , = summ(i) a_i D^(-1/2) (sum_(j in A_i) summ(h) w_(j h) - sum_(j in A_i) summ(h)w_(j h))
    \ , &= summ(i) a_i D^(-1/2) dot 0 , = 0
  ) $
]
#polylux-slide[
#(slim.claim)[
  ER von #Lrw wird aufgespannt durch ${one_A_1, ..., one_A_k}$
  ]
  Sei $f in RR^n$
  
#(slim.recall)[Verall. EW Problem][$Lrw u = lambda u quad <=> quad L u = lambda D u$]
Daher:
$
Lrw f = 0 dot 0 quad <=> quad L f = 0 dot D dot f = 0
$
Also ist der Eigenraum von #Lrw gleich dem Eigenraum von $L$.
]
