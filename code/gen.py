import sympy as sp

def calculate_laplacians(degree_matrix, weight_matrix):
    # Ensure input matrices are sympy matrices
    D = sp.Matrix(degree_matrix)
    W = sp.Matrix(weight_matrix)

    # Inverse degree matrix
    D_inv = sp.diag(*[1 / d for d in D.diagonal()])
    
    # Inverse square root degree matrix
    D_inv_sqrt = sp.diag(*[1 / sp.sqrt(d) for d in D.diagonal()])
    
    # Graph Laplacian
    L = D - W
    
    # Symmetric Normalized Laplacian
    I = sp.eye(D.shape[0])
    L_sym = I - D_inv_sqrt * W * D_inv_sqrt
    
    # Random Walk Laplacian
    L_rw = I - D_inv * W
    
    return L, L_sym, L_rw

# Example usage
degree_matrix = sp.Matrix.diag(4, 3, 2, 1)
weight_matrix = sp.Matrix([
    [0, 2, 1, 1],
    [2, 0, 1, 0],
    [1, 1, 0, 0],
    [1, 0, 0, 0]
])

print("Degree matrix:")
sp.pprint(degree_matrix)
print("\nWeight matrix:")
sp.pprint(weight_matrix)

L, L_sym, L_rw = calculate_laplacians(degree_matrix, weight_matrix)

print("\nGraph Laplacian (L):")
sp.pprint(L)
print("\nSymmetric Normalized Laplacian (L_sym):")
print(L_sym)
print("\nRandom Walk Laplacian (L_rw):")
print(L_rw)

sp.pprint(L_rw * sp.Matrix([[1, 1, 1, 1]]).transpose())
