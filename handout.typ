#import "@local/theorems:0.0.1": init-theorems, zb, proof, rules, base, step, assume, project, break-page-after-chapters

// #set math.equation(numbering: "(1.1)", supplement: "Gleichung")
// #set page(columns: 2)
#set text(10pt)
#let header = [
  Marc Thieme
  #h(1fr)
  #datetime.today().display()
]
#set page(header: header)

#let If = "if"
#let Else = "else"
#let vol = "vol"
#let trans(x) = $#x^T$
#let one = $bb("1")$
#let limm(symbol, var) = $symbol_(var=1)^n$
#let summ = limm.with($sum$)

#let (definition, notation, slim, example, theorem, question) = init-theorems("theorems",
  definition: ("Definition", ),
  notation: ("Notation", ),
  question: ("Frage", gray),
  example: ("Beispiel", gray),
  theorem: ("Satz", ),
)
#let example(..args) = []
#let question(..args) = []

#counter(heading).step()
= Spectral Graph Theory und die Graph Laplacian

#notation[][
  / $x_i in V$: Ein Datenpunkt und gleichzeitig ein Knoten in Graph
  // / $s_(i j) >= 0$: notion of similarity
  // / Similarity Graph: Undirected Graph of datapoints with edges where $s_(i j) > *$
  / $A subset V$: Eine Zusammenhangskomponente des Graphen
  // / $overline(A subset V)$: The complement $V without A$: 
  / $one = vec(1, 1, dots.v, 1)$: Eins-Vektor
  / $f_A = vec(f_1\, ...\, f_n) in R^n$: The vector with entries $f_i = cases(f_i If x_i in A, 0 Else)$
  // / $i in A eq.triple { i | x_i in A}$: The set of indices of the datapoints
]

#definition[$W$][Gewichtsmatrix][
  Eine Matrix $W$ mit nicht-negativen Einträgen, sodass 
  $w_(i j) = cases("Gewicht"(x_i, x_j) "wenn i und j verbunden sind", 0 "sonst")$.

  Da unsere Graphen Schleifenfrei sein werden, gilt: $w_(i i) = 0 quad (i in 1..n)$

  Bei einem ungewichteten Graphen sei $w_(i j) in {0, 1}$.
]

#definition[$D$][Degree Matrix][
  Die Diagonalmatrix $
mat(d_1,\ , 0; \ , dots.down, \ ; 0, \ , d_n),
$ wobei $d_(i)$ der Grad von $x_i$ im Graph ist, heißt Degree-Matrix.
Der Grad ist dabei als $
  d_i = summ(j) w_(i j)
$
definiert
]

// #definition[Size of Vertex sets][
//   + $|A| : "# of vertices in" A$
//   + $vol(A) &: sum_(i in A)d_i$
// ]

== Verschiedene Arten von Ähnlichkeitsgraph
Ein Ähnlichkeitsgraph ist ein Graph, dessen Kanten und Gewichte eine Ähnlichkeitsbeziehung beschreiben.

#definition[$epsilon$-Nachbarschaftsgraph][
  Verbinde alle Knoten mit Distanz weniger als $epsilon$.\
  Dieser Graph macht nur ungewichtet Sinn, da die Gewichte ohnehin alle $epsilon$-nah beieinander lägen.
]

#definition[$k$-nearest neighbor graph][
Jeder Knoten ist mit seinen $k$ nächsten Nachbarn verbunden. Da diese Relation nicht symmetrisch ist, 
muss man hier unterscheiden zwischen:
  / $k$-nearest neighbour graph: Jede gerichtete Kante wird in eine ungerichtete verwandelt
  / mutual $k$-nearest neighbour graph: Nur bidirektionale Kanten werden als ungerichtete Kanten behalten
]

#definition[Fully connected Graph][
  Jeder Knoten ist mit jedem anderen Knoten verbunden. 
  Z.b die Gaussian similarity function $s(x_i, x_j) = exp(-lr(||x_i - x_j ||)^2 / (2 sigma^2))$ where $sigma$ controls the width of the neighborhood
]

#question[][
  Was bedeutet "Lokale Nachbarschaft"?
  Ich finde das weder im Internet. Für mich ist auch nicht intuitiv, warum nicht jede beliebige Ähnlichkeitsfunktion\
  sinnvoll wäre.
  -> Mathematisch geht es schon, aber das ergibt nur dann Sinn
]

= Graph Laplacian
Wir setzen in diesem Kapitel einen
*Ungerichteten Graph* $G$ mit Gewichtsmatrix $W >= 0$ voraus.

#definition[Unnormalized Graph Laplacian][
  $L = D - W$
]

#example[][
$
D := mat(3, 0, 0, 0; 0, 2, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
quad
W := mat(0, 1/2, 1, 2; 1/2, 0, 1, 0; 1, 1, 0, 0; 2, 0, 0, 0)
\
L = D - W = mat(3, -1/2, -1, -2; -1/2, 2, -1, 0; -1, -1, 2, 0; -2, 0, 0, 1)
$
]
#theorem[Graph Laplacian][Eigenschaften][
  + Mit $f in RR^n$: $
    trans(f) L f = 1/2 sum_(i=1)^n sum_(j=1)^n w_(i j) (f_i - f_j)^2
  $
  + $L$ ist symmetrisch und positiv semi-definit.
  + Der kleinste Eigenwert von $L$ ist 0 mit Eigenvektor #one.
  + Eigenwerte von $L$ sind nicht-negativ und reellwertig.
]

#theorem[\# Connected Components][
  Seien $A_1, ..., A_k$ die Zusammenhangskomponenten von $G$.\
  Der Eigenraum des Eigenwerts 0 ist $[one_A_1, ..., one_A_k]$.\
  Insbesondere ist die Multiplizität des Eigenraums zu 0 die Anzahl der Zusammenhangskomponenten $k$.\
]

== Normalisierter Graph Laplacian
#let Lsym = $L_"sym"$
#let Lrw = $L_"rw"$
#definition[Normalisierte Graph Laplacians][
  $
    Lsym &:= D^(-1/2) L D^(-1/2) = I - D^(-1/2) W D^(-1/2)\
    Lrw &:= D^(-1) L = I - D^(-1) W
  $
]
Dabei ist #Lsym eine symmetrische Matrix.

#question[][Aber $D^(-1)$ ist doch nur definiert, wenn jeder Knoten mindestens Grad 1 hat.
Sonst würde man doch durch 0 teilen, oder?
Hierauf wird in dem Paper gar nicht eingegangen. Auf Wikipedia wird hier die Moore-Penrose-Inverse auch verwendet.

-> Handwave
]

#example[][
$
D := mat(3, 0, 0, 0; 0, 2, 0, 0; 0, 0, 2, 0; 0, 0, 0, 1)
quad
W := mat(0, 1/2, 1, 2; 1/2, 0, 1, 0; 1, 1, 0, 0; 2, 0, 0, 0)
quad
L = D - W = mat(3, -1/2, -1, -2; -1/2, 2, -1, 0; -1, -1, 2, 0; -2, 0, 0, 1)
\
Lsym = D^(-1/2) L D^(-1/2) = mat(1, -√6/12, -√6/6, -2√3/3; -√6/12, 1, -1/2, 0; -√6/6, -1/2, 1, 0; -2√3/3, 0, 0, 1)
\
Lrw = D^(-1) L = mat(1, -1/6, -1/3, -2/3; -1/4, 1, -1/2, 0; -1/2, -1/2, 1, 0; -2, 0, 0, 1)
$
]
#theorem[Eigenschaften der normalisierten Graph Laplacians][
  1. Let $f in RR^n$: $
    trans(f) Lsym f = 1/2 summ(i) summ(j) w_(i j) (f_i / sqrt(d_i) - f_j / sqrt(d_j))^2
  $
  2. $lambda$ ist ein Eigenwert von #Lrw mit Eigenvektor $u$ gdw. $lambda$ ein Eigenwert von #Lsym
    mit Eigenvektor $w = D^(1/2) u$ ist.
  3. $lambda$ ist ein Eigenwert von #Lrw mit Eigenwert $u$ gdw. $lambda$ und $u$ das verallgemeinerte
    Eigenwertproblem $L u = lambda D u$ lösen.
  4. 0 ist ein Eigenwert von #Lrw mit $one$ als Eigenvector. 
    0 ist ein Eigenwert von #Lsym mit Eigenvektor $D^(1/2) one$
  5. #Lsym und #Lrw sind positiv semi-definit und haben $n$ nicht-negative reellwertige Eigenwerte.
]
// D^(-1/2) L D^(-1/2) 
#theorem[$Lsym$][#Lrw][\# Conn. Components][
  Sei $G$ ein ungerichteter Graph mit Zusammenhangskomponenten $A_1, ..., A_k$.

  Der Eigenraum von #Lrw wird von den Vektoren ${one_A_1, ..., one_A_k}$
  aufgespannt. 
  Der Eigenraum von #Lsym wird von $quad {D^(1/2) one_A_i | i in 1..k}quad $
  aufgespannt.
]

